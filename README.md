# Try it Live!

To try out this code, check out the following page on the NERDfirst website: https://resources.nerdfirst.net/huffman.html


# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Huffman Coding**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/fPthQE7Li8M/0.jpg)](https://www.youtube.com/watch?v=fPthQE7Li8M "Click to Watch")


# License

All files in this project are licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.